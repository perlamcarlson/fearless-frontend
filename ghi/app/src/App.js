import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="attendees">
          <Route path="attendees" element={<AttendeesList />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentation">
          <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route index element={<MainPage />} />
        
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;